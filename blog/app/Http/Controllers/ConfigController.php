<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfigController extends Controller
{
    //
    public function SignUp(){
        return view('form');
    }

    public function welcome(Request $request){
        //dd($request->all());
        $fnama = $request["fname"];
        $lnama = $request["lname"];
        return view('welcome_to', ["fnama" => $fnama, "lnama" => $lnama]);
    }
}
